<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/auth/register', 'API\V1\Auth\RegisterController@register');
Route::post('v1/auth/login', 'API\V1\Auth\LoginController@login');

Route::middleware('auth:api')->put('v1/user/{user}', 'API\V1\Users\UserController@update')->name('user.update');
Route::middleware('auth:api')->get('v1/user/{user}', 'API\V1\Users\UserController@show')->name('user.show');
Route::middleware('auth:api')->delete('v1/user/{user}', 'API\V1\Users\UserController@destroy')->name('user.destroy');
