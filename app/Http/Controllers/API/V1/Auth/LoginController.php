<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\API\ApiController;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends ApiController
{
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $rules = [
            'email' => 'required|email',
            'password' => 'required'
        ];

        $validator = Validator::make($credentials, $rules);

        if ($validator->fails()) {
            $message = $validator->messages();
            return $this->sendError(false, $message, 400);
        }

        if (!Auth::attempt($credentials)) {
            $message = 'Credentials do not match.';
            return $this->sendError(false, $message, 400);
        }

        $token = Auth::user()->createToken('Passport Token')->accessToken;

        $message = 'Logged in successfully.';
        $data = [
            'token' => $token
        ];

        return $this->sendResponse(true, $message, $data);
    }
}
