<?php

namespace App\Http\Controllers\API\V1\Auth;

use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Support\Facades\Validator;

class RegisterController extends ApiController
{
    function register(Request $request)
    {
        $input = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'c_password' => $request->input('c_password')
        ];

        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
            'c_password' => 'required|same:password'
        ]);

        if ($validator->fails()) {
            $message = $validator->messages();
            return $this->sendError(false, $message, 400);
        }

        $input['password'] = bcrypt($request->input('password'));

        $user = User::create($input);
        $token = $user->createToken('Access Token')->accessToken;

        $message = 'User created successfully.';
        $data = [
            'user' => new UserResource($user),
            'token' => $token
        ];

        return $this->sendResponse(true, $message, $data);
    }
}
