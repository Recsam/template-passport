<?php

namespace App\Http\Controllers\API\V1\Users;

use App\Http\Controllers\API\ApiController;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if ($user == null) {
            $message = 'User not found';
            return $this->sendError(false, $message, 400);
        }

        $message = 'User found';
        $data = [
            'user' => new UserResource($user)
        ];

        return $this->sendResponse(true, $message, $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if ($user == null) {
            $message = 'User not found';
            return $this->sendError(false, $message, 400);
        }

        $input = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        ];

        $validator = Validator::make($input, [
            'name' => '',
            'email' => 'unique:users',
            'password' => ''
        ]);

        if ($validator->fails()) {
            $message = $validator->messages();
            return $this->sendError(false, $message, 400);
        }

        if ($input['name'] != '') {
            $user->name = $input['name'];
        }
        if ($input['email'] != '') {
            $user->email = $input['email'];
        }
        if ($input['password'] == '') {
            $user->password = bcrypt($input['password']);
        }

        $user->save();

        $message = 'User updated successfully.';
        $data = [
            'user' => new UserResource($user)
        ];

        return $this->sendResponse(true, $message, $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user == null) {
            $message = 'User not found';
            return $this->sendError(false, $message, 400);
        }
        $user->delete();
        $message = 'User deleted successfully.';
        $data = [
            'user' => new UserResource($user)
        ];

        return $this->sendResponse(true, $message, $data);
    }
}
