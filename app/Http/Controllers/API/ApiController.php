<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function sendResponse($status, $message, $result, $code = 200)
    {
        return response()->json([
            'code' => $code,
            'status' => $status,
            'message' => $message,
            'data' => $result
        ], 200);
    }

    public function sendError($status, $message, $code = 404)
    {
        return response()->json([
            'code' => $code,
            'status' => $status,
            'message' => $message
        ], 200);
    }

}
